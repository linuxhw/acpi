[TOC]

ACPI Tables
===========

This is a repository of decoded ACPI tables for various computers collected
by Linux users at https://linux-hardware.org.

Everyone can contribute to this repository by uploading probes of their computers
by the [hw-probe](https://github.com/linuxhw/hw-probe) tool:

    sudo hw-probe -all -upload -dump-acpi

About
-----

The structure of the directory is the following:

    {TYPE}/{VENDOR}/{MODEL PREFIX}/{MODEL}/{HWID}

    ( e.g. Notebook/Lenovo/G570/G570 20079/87FB42D5B9E2 )

Notebooks
---------

| MFG              | Model                  | HWID         |
|------------------|------------------------|--------------|
| ASUSTek Computer | 1215N                  | 9A77F30CC078 |
| ASUSTek Computer | A3L                    | F71442C2DD56 |
| ASUSTek Computer | E403NA                 | E3A7CFB90011 |
| ASUSTek Computer | G752VS                 | 2C3A54B9621B |
| ASUSTek Computer | K54HR                  | CDCECF3994DD |
| ASUSTek Computer | N53Jf                  | 72185E9A7C5E |
| ASUSTek Computer | N53Ta                  | 44FD90565A2A |
| ASUSTek Computer | X200CA                 | 8CF325A2BCD9 |
| ASUSTek Computer | X71SL                  | C5769FF2BEDC |
| ASUSTek Computer | X750JB                 | F58754409DC7 |
| Aava Mobile Oy   | INARI8-LTBN-1          | A658B5B6E0CB |
| Acer             | Aspire E1-571G         | 56A1CBDE0299 |
| Acer             | Aspire E1-571G         | 80E357275D08 |
| Apple            | MacBookPro14,3         | 91077C25D705 |
| Dell             | Inspiron 3558          | 7C3F03888987 |
| Dell             | Latitude E6520         | C019B771B42C |
| Dell             | Latitude E6530         | 8B5EC6878A4B |
| Dell             | Latitude E6540         | 478086798418 |
| Dell             | Precision 7530         | 5B887671E31F |
| Hewlett-Packard  | 15                     | 07149FC6072A |
| Hewlett-Packard  | Compaq 6730b           | 795F37601A0A |
| Hewlett-Packard  | ENVY m6                | 4EB247E5B835 |
| Hewlett-Packard  | ENVY x360 Convertib... | 92AD36EABAA5 |
| Hewlett-Packard  | EliteBook 2740p        | 0A6CC5DD2EF6 |
| Hewlett-Packard  | EliteBook 8740w        | 6EA92286C100 |
| Hewlett-Packard  | EliteBook 8740w        | C619FE23190E |
| Hewlett-Packard  | Pavilion dv8           | D8B9EB76B074 |
| Hewlett-Packard  | ProBook 470 G0         | 7206C3577DFF |
| Hewlett-Packard  | Stream 13              | ED889F5F2A86 |
| Hewlett-Packard  | TouchSmart tx2         | 6909B67B540B |
| Insyde           | BayTrail               | 9F287627613B |
| Lenovo           | Flex 2-15              | 8F84E14F5157 |
| Lenovo           | Flex 3-1480 80R3       | 005ABC827F1B |
| Lenovo           | G570 20079             | 45D05063F068 |
| Lenovo           | G570 20079             | 92B1F90D7EAE |
| Lenovo           | IdeaPad 110-15IBR 80T7 | 378170405FFB |
| Lenovo           | IdeaPad U310           | 3AC887234B79 |
| Lenovo           | ThinkPad T61 76641FG   | 765F0725EA93 |
| Lenovo           | ThinkPad X380 Yoga ... | 5EDC3F139B76 |
| Lenovo           | ThinkPad X61 Tablet... | 175B115C38BD |
| Lenovo           | ThinkPad X61 Tablet... | A54FD909734A |
| MSI              | GT70                   | D4EFA09F0CFD |
| MSI              | MS-N031                | 237F541314EF |
| MSI              | PR200                  | 94CECEDC956C |
| MSI              | X460-X460DX            | 7AA60F771207 |
| Samsung Elect... | 300V3A-300V4A-300V5... | 35CC56EF66E3 |
| Samsung Elect... | 305U1A                 | 09E2C5E73434 |
| Samsung Elect... | 355V4C-355V4X-355V5... | 51CDAFB26356 |
| Samsung Elect... | Q310                   | 94C0252E4BC9 |
| Samsung Elect... | RF510-RF410-RF710      | 0D3B24930741 |
| Samsung Elect... | RF510-RF410-RF710      | 5BEB13CF77EB |
| Sony             | SVE1713S1RW            | 506CDC50E671 |
| Timi             | TM1701                 | 6F3EF48DD098 |
| Toshiba          | Satellite A135         | D67BA1A5ADD8 |
| Toshiba          | Satellite Pro L630     | 2BCF481E15A0 |
| Toshiba          | Satellite Pro L630     | E4A738095425 |
| Toshiba          | TECRA A50-A            | 36303D77C8F0 |

Desktops
--------

| MFG              | Model                  | HWID         |
|------------------|------------------------|--------------|
| ASRock           | G31M-S                 | 53A47C01E2B6 |
| ASUSTek Computer | All Series             | 21299B1F4635 |
| ASUSTek Computer | F1A75-M LE             | 0DD8332C0CDD |
| ASUSTek Computer | M4A78T-E               | 83CD211F5082 |
| ASUSTek Computer | P8P67                  | 153751203A61 |
| ASUSTek Computer | PRIME A320M-K          | 7A97D933DB40 |
| ASUSTek Computer | PRIME B250M-PLUS       | 55D7B7DDD6EA |
| ASUSTek Computer | PRIME X370-PRO         | A169FD97AB0E |
| ASUSTek Computer | SABERTOOTH 990FX       | 3D77ED0B498A |
| ASUSTek Computer | SABERTOOTH 990FX       | B015992EFDE5 |
| Acer             | Aspire R3600           | C85DE8E38D84 |
| Acer             | Aspire Z3-615          | E37DB50E1157 |
| Aquarius         | Aquarius Pro P30 S75   | 0485A3072F96 |
| Aquarius         | Aquarius Pro P30 S75   | 9753F4AD3EFB |
| Aquarius         | Aquarius Pro, Std, ... | C81391734654 |
| Biostar          | TB250-BTC PRO          | 0DD393D67EFC |
| Gigabyte Tech... | B450M DS3H             | D9E9E66D46A4 |
| Gigabyte Tech... | B75M-D3V               | 98E089E19203 |
| Gigabyte Tech... | GA-78LMT-USB3          | E4D07F2A4E6F |
| Gigabyte Tech... | Q87M-D2H               | 4A407DB4786C |
| Gigabyte Tech... | Q87M-D2H               | 6166ADBA5434 |
| Hewlett-Packard  | 750-467c               | D213850B6974 |
| Hewlett-Packard  | Compaq 6005 Pro MT PC  | 86BB28345D1D |
| Hewlett-Packard  | EliteDesk 800 G1 SFF   | F13506CA489E |
| MSI              | MS-7721                | 3EF15301B047 |
| MSI              | MS-7752                | 14FAD414B696 |
| TONK             | C31                    | 7F7E8D75C7FF |
| TONK             | TN1402                 | ABC6298CB633 |
| ZOTAC            | NM10                   | 27F12A855946 |

Servers
-------

| MFG              | Model                  | HWID         |
|------------------|------------------------|--------------|
| AIC              | FB201-LX               | E9F0ABF1FB7A |
| DEPO Computers   | Super Server           | 5ED617DD5961 |
| Hewlett-Packard  | ProLiant DL380e Gen8   | CB05571909C8 |
| STSS             | Flagman TP100.3        | 34DC8A4302D7 |
| Supermicro       | X8DTN+-F               | 319567CDA949 |

